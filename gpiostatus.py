#!/usr/bin/env python3

""" 
MIT License

Copyright (c) 2017 Benargee

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 """

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)

print(GPIO.RPI_INFO['TYPE'], end="")
print(" - RPi.GPIO V", end="")
print(GPIO.VERSION)

for i in range(1,41):
	pincol = "\033[1;37;40m"
	fnccol = "\033[1;37;40m"
	nrmcol = "\033[1;37;40m"
	state = " "

	if i in [1,17]:
		type = "33VDC"
		pincol = "\033[0;31;40m"#red
		fnccol = "\033[0;31;40m"
	elif i in [2,4]:
		type = "5VDC"
		pincol = "\033[1;31;40m"#bold red
		fnccol = "\033[1;31;40m"
	elif i in [6,9,14,20,25,30,34,39]:
		type = "GND"
		pincol = "\033[1;30;40m"
		fnccol = "\033[1;30;40m"
	elif i in [27,28]:
		type = "I2CEEPROM"
		pincol = "\033[1;33;40m"#bold yellow
		fnccol = "\033[1;33;40m"	
	else:
		func = GPIO.gpio_function(i)
		pincol = "\033[1;32;40m"
		if func == GPIO.IN:
			type = "GPIO.IN"
			fnccol = "\033[1;32;40m"#green
		elif func == GPIO.OUT:
			type = "GPIO.OUT"
			fnccol = "\033[1;36;40m"#cyan
		elif func == GPIO.SPI:
			type = "GPIO.SPI"
			fnccol = "\033[1;35;40m"#purple
		elif func == GPIO.I2C:
			type = "GPIO.I2C"
			fnccol = "\033[1;34;40m"#bold blue
		elif func == GPIO.HARD_PWM:
			type = "GPIO.HARD_PWM"
			fnccol = "\033[0;34;40m"#blue
		elif func == GPIO.SERIAL:
			type = "GPIO.SERIAL"
			fnccol = "\033[0;33;40m"#yellow
		elif func == GPIO.UNKNOWN:
			type = "GPIO.UNKNOWN"
			fnccol = "\033[1;37;41m"#red background
		else:
			type = "ERROR"
			fnccol = "\033[1;37;41m"#red background

	print(' {:s}PIN:{:d}{:s} : {:s}{:s}{:s}{:s} '.format(pincol,i,nrmcol,fnccol,type,nrmcol,state))

#GPIO.cleanup()

